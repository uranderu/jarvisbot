# Jarvis BOT

## Functionality

- Calculates chance of rain based on a location from the OpenWeatherAPI.
- Communicates everything (errors too) through Telegram.
- Built with Rust, so it is BLAZINGLY FAST!!

## How to use it?

This bot runs a cron job internally so that it runs every day at 9AM. Because it is built in Rust, the resource cost of
this should be very minimal. I use environment variables for everything, so you could simply replace those. Then it
should also work for your location and via your Telegram bot. There is a pipeline which builds the container and
publishes it to a public DockerHub repo.

The used to run on an AWS Lambda function. Go back before the Rust branch merge, and you'll find it. I used environment
variables for everything, so you could simply replace those. The environment variables in the script itself I set
through the AWS console. There is a pipeline which builds the container, publishes it to a private ECR repo and updates
the function accordingly.