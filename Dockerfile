FROM rust:latest as builder
WORKDIR /usr/src/jarvisbot
COPY . .
RUN cargo install --path . --force

FROM debian:11
RUN apt update && apt install ca-certificates -y
COPY --from=builder /usr/local/cargo/bin/jarvisbot /usr/local/bin/jarvisbot

ENV FORECAST_URL=""
ENV TG_BOT_URL=""

CMD ["jarvisbot"]
