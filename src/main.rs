use std::env;
use std::process;

use cronjob::CronJob;
use reqwest;

fn send_message(message: &str) {
    let bot_url = format!("{}{}", env::var("TG_BOT_URL").unwrap(), message);
    reqwest::blocking::get(&bot_url).unwrap();
}

struct DailyRainForecast {
    forecasts: Option<serde_json::Value>,
}

impl DailyRainForecast {
    fn get_forecast(&mut self) {
        let forecast_url = env::var("FORECAST_URL").unwrap();

        let api_response = reqwest::blocking::get(&forecast_url);
        match api_response {
            Ok(response) => {
                self.forecasts = Some(response.json().unwrap());
            }
            Err(e) => {
                let message = format!("Sorry sir, error: {} has occurred at the weather API.", e);
                send_message(&message);
                process::exit(1);
            }
        }
    }

    fn calculate_forecast(&self) {
        let mut rain_amount = 0.0;
        let mut rain_chance = 0.0;

        if let Some(forecasts) = &self.forecasts {
            if let Some(forecast_list) = forecasts["list"].as_array() {
                for forecast in forecast_list {
                    if let Some(rain) = forecast["rain"]["3h"].as_f64() {
                        rain_amount += rain;
                    }
                    if let Some(pop) = forecast["pop"].as_f64() {
                        rain_chance += pop;
                    }
                }

                rain_chance = (rain_chance * 100.0) / 8.0; // Average of 24 hours.
                rain_amount = rain_amount.round();
                rain_chance = rain_chance.round();

                let message = format!("Good morning sir, there is an average rain probability of {:.2}% today. With a total volume of {:.2}mm.", rain_chance, rain_amount);
                send_message(&message);
            }
        }
    }

    fn new() {
        let mut forecast = DailyRainForecast { forecasts: None };
        forecast.get_forecast();
        forecast.calculate_forecast();
    }
}

fn on_cron(name: &str) {
    println!("Start CRON: {}", name);
    DailyRainForecast::new();
    println!("End CRON: {}", name);
}

fn main() {
    println!("JarvisBOT initialized");
    let mut cron = CronJob::new("Daily run", on_cron);
    cron.hours("9");
    cron.minutes("0");
    cron.seconds("0");
    cron.start_job();
}